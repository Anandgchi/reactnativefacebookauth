/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import RNAccountKit from 'react-native-facebook-account-kit'
import Axios from 'axios';


export default class App extends Component{

componentDidMount(){
  RNAccountKit.configure({
    responseType: 'code', // 'token' by default,
    initialPhoneCountryPrefix: '+977', // autodetected if none is provided
    // receiveSMS: true|false, // true by default,
    // countryWhitelist: ['AR'], // [] by default
    // countryBlacklist: ['US'], // [] by default
    defaultCountry: 'NP',
    // theme: {...}, // for iOS only, see the Theme section
    // viewControllerMode: 'show'|'present' // for iOS only, 'present' by default
    // getACallEnabled: true|false
  })
    // Shows the Facebook Account Kit view for login via SMS
RNAccountKit.loginWithPhone()
.then(async(token) => {
  if (!token) {
    console.log('Login cancelled')
  } else {
    console.log(`Logged with phone. Token: ${JSON.stringify(token)}`);
    
    var getResults = this.sendRequestForPhoneNumber(token.code);
   console.log('Here is the verified phone number',getResults);
  }
})
}

  sendRequestForPhoneNumber = async() =>{
    try {
    var access_token_data = ['AA','411550169391044','a702bb4d451b8fb8713bf6475bda7dd1']
    var access_token = access_token_data.join('|');
    var token_exchange_url = 'https://graph.accountkit.com/v1.1/access_token?grant_type=authorization_code&code=${token}&access_token=${access_token}';
    let getTokens = await Axios(token_exchange_url);
    let  getDetailsUrl ='https://graph.accountkit.com/v1.1/me?access_token=${getTokens.data.access_token}';
    let getDetauilsUser= await Axios(getDetailsUrl);
    return getDetauilsUser.data;
} catch ({error}) {
  console.log('Error from catch block',error);
}
  
  }
  render() {
              // Configures the SDK with some options
       
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Congratulations you have successfully verified the account</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
